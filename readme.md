# Je lis puis j'écris des mots #

Adapation de l'application éducative PragmaTICE "Je lis puis j'écris" de la compilation d'applications Clicmenu.

L'application originale a été adaptée en deux applications, "Je lis puis j'écris des mots" (lis-ecris), "Je lis puis j'écris des phrases" (lis-ecris2).

L'activité consiste à réécrire un mot présenté pendant un temps limité.

L'application originale propose des exercices sur les thèmes suivants :

- mots de l'échelle Dubois-Buyse
- mots invariables
- mots représentant les nombres

Cette reprise ajoute d'autres fonctionnalités :

- mots de l'échelle Dubois-Buyse actualisée
- mots de l'échelle Dubois-Buyse actualisée par phonèmes pour le cycle 2
- mots de l'échelle Dubois-Buyse actualisée par phonèmes pour le cycle 3

L'application peut être testée en ligne [ici](https://primtux.fr/applications/lis-ecris/index.html)